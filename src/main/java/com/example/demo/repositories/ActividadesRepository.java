package com.example.demo.repositories;

import java.util.ArrayList;

import com.example.demo.models.ActividadesModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActividadesRepository extends CrudRepository<ActividadesModel, Long> {
    public abstract ArrayList<ActividadesModel> findByPrioridad(Integer prioridad);

}