package com.example.demo.models;

import javax.persistence.*;

@Entity
@Table(name = "actividades")
public class ActividadesModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    private String descripcion;
    // private String email;
    // private Integer prioridad;

    // public void setPrioridad(Integer prioridad){
    //     this.prioridad = prioridad;
    // }

    // public Integer getPrioridad(){
    //     return prioridad;
    // }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    // public String getEmail() {
    //     return email;
    // }

    // public void setEmail(String email) {
    //     this.email = email;
    // }
    
}