package com.example.demo.services;

import java.util.ArrayList;
import java.util.Optional;

import com.example.demo.models.ActividadesModel;
import com.example.demo.repositories.ActividadesRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActividadesService {
    @Autowired
    ActividadesRepository usuarioRepository;
    
    public ArrayList<ActividadesModel> obtenerUsuarios(){
        return (ArrayList<ActividadesModel>) usuarioRepository.findAll();
    }

    public ActividadesModel guardarUsuario(ActividadesModel usuario){
        return usuarioRepository.save(usuario);
    }

    public Optional<ActividadesModel> obtenerPorId(Long id){
        return usuarioRepository.findById(id);
    }


    public ArrayList<ActividadesModel>  obtenerPorPrioridad(Integer prioridad) {
        return usuarioRepository.findByPrioridad(prioridad);
    }

    public boolean eliminarUsuario(Long id) {
        try{
            usuarioRepository.deleteById(id);
            return true;
        }catch(Exception err){
            return false;
        }
    }


    
}